package handler

import (
	"context"
	"encoding/json"
	"github.com/go-chi/chi"
	_log "github.com/rs/zerolog/log"
	"net/http"

	"gitlab.com/mind-framework/core/mind-core-api/middlewares"
	"gitlab.com/mind-framework/core/mind-core-api/model"
	"gitlab.com/mind-framework/core/mind-core-api/render"
)

var log = _log.With().Str("pkg", "handler").Logger()

//ctxKeyType es un nuevo tipo utilizado para generar keys para los contextos
type ctxKeyType int

const (
	modelCtxKey      ctxKeyType = iota
	modelCtxPkValues ctxKeyType = iota
)

//NewCRUDHandler retorna un handler para ABM Genéricos
func NewCRUDHandler(m model.NewModeler) *CRUDHandler {

	if m == nil {
		log.Fatal().Msg("NewHandler: Se esperaba un Modeler")
	}
	r := &CRUDHandler{NewModel: m}
	return r
}

//CRUDHandler Estructura con lo métodos para la recepción de los requests al recurso
type CRUDHandler struct {
	// Model model.Modeler
	NewModel model.NewModeler
}

// Routes define las rutas para el recurso
func (h CRUDHandler) Routes() chi.Router {
	r := chi.NewRouter()
	//creo una instancia del model, para obtener el nombre de la clave primaria
	m := h.NewModel()
	pks := m.GetPrimaryKeys()

	r.Get("/", h.List)
	r.Post("/", h.Create)

	r.Route("/{"+pks[0]+"}", func(r chi.Router) {
		r.Use(GetPkValuesMdw(h.NewModel))
		r.Use(GetModelCtxMdw(h.NewModel))
		r.Get("/", h.Get)
		r.Patch("/", h.Update)
		r.Delete("/", h.Delete)
	})

	return r
}

// List devuelve la lista completa de recursos
func (h CRUDHandler) List(w http.ResponseWriter, r *http.Request) {
	log = log.With().Str("ctx", "List").Logger()
	rp := middlewares.GetContextParams(r)
	log.Trace().Interface("data", rp).Msg("requestParams")
	qp := rp.QueryParams
	log.Debug().Interface("data", qp).Msg("queryParams")

	// var offset uint
	limit := qp.Get("limit")
	delete(qp, "limit")

	offset := qp.Get("offset")
	delete(qp, "offset")

	sort := qp["sort"]
	delete(qp, "sort")

	filters := make(map[string][]string)
	for i := range qp {
		log.Trace().Interface("filter", i).Msg("")
		filters[i] = qp[i]
		// filters = appen()

	}

	m := h.NewModel()
	result, err := m.List(sort, limit, offset, filters)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	data := result.(model.Modeler).GetData()
	meta := result.(model.Modeler).GetMeta()

	log.Trace().Str("handler", "List").Interface("result", data).Msg("")

	render.Render(w, r, render.NewSuccessResponseWithMeta(data, meta))
}

//Create maneja las solicitudes de creación de un recurso
func (h CRUDHandler) Create(w http.ResponseWriter, r *http.Request) {
	log = log.With().Str("ctx", "Crate").Logger()
	rp := middlewares.GetContextParams(r)
	log.Trace().Interface("requestParams", rp).Msg("Parámetros recibidos")
	payload := rp.RequestBody.Data
	log.Trace().Interface("data", payload).Msg("Datos a Insertar")

	m := h.NewModel()

	result, err := m.Insert(payload)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	data := result.(model.Modeler).GetData()
	meta := result.(model.Modeler).GetMeta()

	log.Debug().Str("handler", "Create").Interface("result", data).Msg("")

	render.Render(w, r, render.NewSuccessResponseWithMeta(data, meta))
}

//Get Maneja las solicitudes de obtención de un recurso
func (h CRUDHandler) Get(w http.ResponseWriter, r *http.Request) {
	log = log.With().Str("ctx", "Get").Logger()
	record := GetContextModel(r)

	data := record.(model.Modeler).GetData()
	meta := record.(model.Modeler).GetMeta()

	render.Render(w, r, render.NewSuccessResponseWithMeta(data, meta))
}

//Update maneja las solicitudes de Actualización de Recursos
func (h CRUDHandler) Update(w http.ResponseWriter, r *http.Request) {
	log = log.With().Str("ctx", "Update").Logger()
	rp := middlewares.GetContextParams(r)
	log.Trace().Interface("data", rp).Msg("Parámetros recibidos")
	payload := rp.RequestBody.Data
	log.Trace().Interface("data", payload).Msg("Datos a Actualizar")

	var err error

	ids := GetContextPkValues(r)
	m := GetContextModel(r)
	d := m.GetData()

	byteData, err := json.Marshal(payload)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	err = json.Unmarshal(byteData, &d)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}

	result, err := m.Update(d, ids...)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	data := result.(model.Modeler).GetData()
	meta := result.(model.Modeler).GetMeta()

	log.Debug().Str("handler", "Update").Interface("result", data).Msg("")

	render.Render(w, r, render.NewSuccessResponseWithMeta(data, meta))
}

//Delete Maneja las solicitudes de eliminación de recursos
func (h CRUDHandler) Delete(w http.ResponseWriter, r *http.Request) {
	log = log.With().Str("ctx", "Delete").Logger()
	var err error
	m := h.NewModel()

	ids := GetContextPkValues(r)

	err = m.Delete(ids...)

	// if id := chi.URLParam(r, "id"); id != "" {
	// 	err = m.Delete(id)
	// }

	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}

	render.Render(w, r, render.NewSuccessResponse("Se eliminó el registro"))
}

//GetModelCtxMdw es un middleware que obtiene realiza un Get en el Modeler ingresado como parámetro
// El resultado es almacenado en el cxt y puede ser recuperado con GetContextModel
func GetModelCtxMdw(NewModel model.NewModeler) func(next http.Handler) http.Handler {
	log.Debug().Msg("Atachando Middleware GetModelCtxMdw")
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			var err error
			m := NewModel()

			pks := m.GetPrimaryKeys()
			log.Debug().Str("contexto", "Middleware").Interface("pk", pks).Msg("Leyendo Primary Keys del Modelo")

			ids := GetContextPkValues(r)

			_, err = m.Get(ids...)

			if err != nil {
				log.Error().Err(err).Msg("no se encontró el recurso especificado")
				render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
				return
			}

			// log.Debug().Interface("data", cliente).Msg("Obteniendo Recurso")

			ctx := context.WithValue(r.Context(), modelCtxKey, m)
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}

//GetContextModel devuelve el modelo almacenado en cxt por el middleware GetModelCtx
func GetContextModel(r *http.Request) model.Modeler {
	ctx := r.Context()
	v := ctx.Value(modelCtxKey)
	if v == nil {
		return nil
	}
	return v.(model.Modeler)
}

//GetPkValuesMdw busca los valores de las claves primarias del modelo y las almacena en el contexto
func GetPkValuesMdw(NewModel model.NewModeler) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// var err error
			m := NewModel()

			pks := m.GetPrimaryKeys()

			var ids []interface{}
			for pk := range pks {
				if id := chi.URLParam(r, pks[pk]); id != "" {
					ids = append(ids, id)
				} else {
					log.Error().Interface("key", pks[pk]).Msg("No se recibió un valor para la clave especificada")
					render.Render(w, r, render.NewGenericErrorResponse("Error al obtener el recurso especificado"))
					return
				}
			}

			ctx := context.WithValue(r.Context(), modelCtxPkValues, ids)
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}

//GetContextPkValues devuelve el modelo almacenado en cxt por el middleware GetPkValuesMdw
func GetContextPkValues(r *http.Request) []interface{} {
	ctx := r.Context()
	v := ctx.Value(modelCtxPkValues)
	if v == nil {
		return nil
	}
	return v.([]interface{})
}
