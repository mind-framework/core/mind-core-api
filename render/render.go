package render

import (
	"github.com/go-chi/render"
	"net/http"
)

//Render un simple wrapper de chi render.Render, para que el que usa este paquete no necesite importarlo
func Render(w http.ResponseWriter, r *http.Request, v render.Renderer) error {
	return render.Render(w, r, v)
}

// NewSuccessResponse Devuelve un Renderer para una respuesta del tipo Success
// payload será enviado dentro de la propiedad "data" del objeto de respuesta
func NewSuccessResponse(data interface{}) *Response {
	return &Response{
		HTTPStatusCode: 200,
		Success:        true,
		Payload:        data,
	}
}

// NewSuccessResponseWithMeta Devuelve un Renderer para una respuesta del tipo Success
// payload será enviado dentro de la propiedad "data" del objeto de respuesta
// el segundo parámetro es la información que se adjunta en metadata
func NewSuccessResponseWithMeta(data interface{}, meta interface{}) *Response {
	return &Response{
		HTTPStatusCode: 200,
		Success:        true,
		Payload:        data,
		Metadata:       meta,
	}
}

//NewGenericErrorResponse devuelve un Renderer para una respuesta genércia de error
func NewGenericErrorResponse(message string) *Response {
	return &Response{
		HTTPStatusCode: 500,
		Success:        false,
		Message:        message,
	}
}

//NewErrorNotFoundResponse devuelve un error 404
func NewErrorNotFoundResponse(message string) *Response {
	return &Response{
		HTTPStatusCode: 404,
		Success:        false,
		Message:        message,
	}
}

//NewErrorBadRequestResponse devuelve un error 400
func NewErrorBadRequestResponse(message string) *Response {
	return &Response{
		HTTPStatusCode: 400,
		Success:        false,
		Message:        message,
	}
}

//Response estructura que contiene los datos a devolver al usuario final
type Response struct {
	HTTPStatusCode int `json:"code,omitempty"`

	//AppErrorCode contiene el error interno de la aplicación
	AppErrorCode int64       `json:"errorCode,omitempty"`
	Message      string      `json:"message,omitempty"`
	Success      bool        `json:"success"`
	Payload      interface{} `json:"data,omitempty"`
	Metadata     interface{} `json:"meta,omitempty"`
}

//Render Ajusta los a enviar en caso de error
func (e *Response) Render(w http.ResponseWriter, r *http.Request) error {
	if e.HTTPStatusCode == 0 {
		e.HTTPStatusCode = 200
	}
	render.Status(r, e.HTTPStatusCode)
	return nil
}

//Metadata contiene información adicional al payload, por ejemplo, cantidad de registros
type Metadata struct {
}
