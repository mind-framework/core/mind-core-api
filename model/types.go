package model

import (
	"database/sql/driver"
	"encoding/json"
	"strings"
	// "errors"
	// "fmt"
	// "io"
	// "log"
	// "net"
	// "os"
	// "time"
)

//WeekDays - Estructura con los días de la semana
type WeekDays struct {
	Mon bool
	Tue bool
	Wed bool
	Thu bool
	Fri bool
	Sat bool
	Sun bool
}

//Value - Devuelve los días de la semana como JSON
func (wd *WeekDays) Value() (driver.Value, error) {
	if wd.IsNull() {
		return nil, nil
	}

	return json.Marshal(wd)
}

//Scan - lee los días de la seman desde un json o de un array
func (wd *WeekDays) Scan(value interface{}) error {

	if b, ok := value.([]byte); ok == true {
		json.Unmarshal(b, &wd)
	} else {
		wd.fromSlice(value)
	}

	return nil
}

//MarshalJSON Devuelve una representación JSON de los dias de la semana
func (wd WeekDays) MarshalJSON() ([]byte, error) {
	var i []string
	if wd.Mon {
		i = append(i, "Lun")
	}
	if wd.Tue {
		i = append(i, "Mar")
	}
	if wd.Wed {
		i = append(i, "Mié")
	}
	if wd.Thu {
		i = append(i, "Jue")
	}
	if wd.Fri {
		i = append(i, "Vie")
	}
	if wd.Sat {
		i = append(i, "Sáb")
	}
	if wd.Sun {
		i = append(i, "Dom")
	}

	return json.Marshal(i)

}

//UnmarshalJSON método que realiza la conversión desde JSON
func (wd *WeekDays) UnmarshalJSON(data []byte) error {
	var list interface{}

	*wd = WeekDays{false, false, false, false, false, false, false}

	if err := json.Unmarshal(data, &list); err == nil {
		wd.fromSlice(list)

	}

	return nil
}

/*
	carga la estructura desde una lista de días recibidas en un slice
	el formato de list debe ser un slice con el formato: ["Lun", ... ,"Dom"]
*/
func (wd *WeekDays) fromSlice(list interface{}) error {

	*wd = WeekDays{false, false, false, false, false, false, false}

	for _, c := range list.([]interface{}) {
		switch strings.ToLower(c.(string)) {
		case "lun", "lunes", "mon", "monday":
			wd.Mon = true
		case "mar", "martes", "tue", "tuesday":
			wd.Tue = true
		case "mie", "mié", "miercoles", "miércoles", "wed", "wednesday":
			wd.Wed = true
		case "jue", "jueves", "thu", "thursday":
			wd.Thu = true
		case "vie", "viernes", "fri", "friday":
			wd.Fri = true
		case "sab", "sáb", "sábado", "sat", "saturday":
			wd.Sat = true
		case "dom", "domingo", "sun", "sunday":
			wd.Sun = true

		}
	}

	return nil
}

//IsNull devuelve true si el campo es nulo
func (wd WeekDays) IsNull() bool {
	return !wd.Mon && !wd.Tue && !wd.Wed && !wd.Thu && !wd.Fri && !wd.Sat && !wd.Sun
}

/*  ********************************* JSON SLICE ************************************* */

//Slice Tipo de dato array JSON
//Ejemplo: [{"nombre":"valor","apellido":"valor"},{"nombre":"valor","apellido":"valor"}]
type Slice []string

//Value devuelve la versión json de la variable
func (j Slice) Value() (driver.Value, error) {

	if j.IsNull() {
		return nil, nil
	}

	return json.Marshal(j)
}

//Scan - devuelve un array de un objeto json
func (j *Slice) Scan(value interface{}) error {

	if b, ok := value.([]byte); ok == true {
		json.Unmarshal(b, &j)
	} else {
		var m []byte
		var err2 error

		if m, err2 = json.Marshal(value); err2 == nil {
			json.Unmarshal(m, &j)
		} else {
			return err2
		}
	}

	return nil
}

//IsNull - Devuelve true si la variable está vacía
func (j Slice) IsNull() bool {
	return j == nil || len(j) == 0
}

/*  ********************************* JSON SLICE de objectos************************************* */

//SliceObjects Tipo de dato array JSON
//Ejemplo: [{"nombre":"valor","apellido":"valor"},{"nombre":"valor","apellido":"valor"}]
type SliceObjects []map[string]interface{}

//Value devuelve la versión json de la variable
func (j SliceObjects) Value() (driver.Value, error) {

	if j.IsNull() {
		return nil, nil
	}

	return json.Marshal(j)
}

//Scan - devuelve un array de un objeto json
func (j *SliceObjects) Scan(value interface{}) error {

	if b, ok := value.([]byte); ok == true {
		json.Unmarshal(b, &j)
	} else {
		var m []byte
		var err2 error

		if m, err2 = json.Marshal(value); err2 == nil {
			json.Unmarshal(m, &j)
		} else {
			return err2
		}
	}

	return nil
}

//IsNull - Devuelve true si la variable está vacía
func (j SliceObjects) IsNull() bool {
	return j == nil || len(j) == 0
}
