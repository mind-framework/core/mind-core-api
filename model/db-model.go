package model

import (
	// "github.com/rs/zerolog/log"
	"errors"
	"github.com/jmoiron/sqlx"
	"regexp"
	"strings"
	// "fmt"

	// migracion
	"database/sql"
	"github.com/golang-migrate/migrate"
	"github.com/golang-migrate/migrate/database/mysql"
	//importando source file
	_ "github.com/golang-migrate/migrate/source/file"
)

var (
	//DB Referencia a una instancia de sqlx conectada a inicializada
	DB *sqlx.DB
)

func init() {
	log.Info().Msg("Inicializando Modelo")

}

// Init inicializa el paquete model
func Init(dbType, connStr string) error {
	log = log.With().Str("ctx", "DB").Logger()
	log.Debug().Interface("data", DB).Msg("Incializando model.DB")

	log.Info().Msg("Conectando con la Base de Datos")
	log.Trace().Str("connStr", connStr).Msg("Datos de Conexión:")
	db, err := sqlx.Connect(dbType, connStr)
	if err != nil {
		log.Fatal().Err(err).Str("cat", "database").Msg("No se puedo conectar a la Base de Datos")
	}

	if err := db.Ping(); err != nil {
		log.Fatal().Err(err).Str("cat", "database").Msg("No se puedo conectar a la Base de Datos")
	}

	DB = db

	//migraciónd e BD
	log.Debug().Msg("Iniciando Migración")
	connMigration, _ := sql.Open("mysql", connStr+"?multiStatements=true")
	driver, _ := mysql.WithInstance(connMigration, &mysql.Config{})
	m, err := migrate.NewWithDatabaseInstance(
		"file://db/migrate",
		"mysql",
		driver,
	)
	if err != nil {
		log.Fatal().Err(err).Str("cat", "migrate").Msg("no se pudo realizar migración")
	}
	if err := m.Up(); err != nil && err.Error() != "no change" {
		log.Fatal().Err(err).Str("cat", "migrate").Msg("Error en Up")
	}
	log.Debug().Msg("Fin de  Migración")

	return nil
}

//dbSelect es una estructura que permite serializar la ejecución de Select
type dbSelect struct {
	// TableName string
	dest       Modeler
	cols       string
	table      string
	leftJoin   string
	on         string
	sort       []string
	limit      string
	groupby    []string
	whereSQL   []string
	whereBinds []interface{}
	offset     string
}

//Select devuelve una Referencia a un dbSelect
//model es una referencia a una estructura que implemente la interfaz Modeler
func Select(cols string) *dbSelect {
	return &dbSelect{
		cols:  cols,
		limit: "1000",
	}
}

//Into establece el destino del resultado del select
func (m *dbSelect) Into(dest Modeler) *dbSelect {
	m.dest = dest
	return m
}

//
func (m *dbSelect) From(table string) *dbSelect {
	m.table = table
	return m
}

//OrderBy recibe una lista de campos por los cuales se debe realizar el OrderBy
// si alguno de los campos contiene un prefijo "-" se ordena descendente por ese campo
func (m *dbSelect) OrderBy(sort []string) *dbSelect {
	//_sort contiene los campos válidos para sort
	var _sort []string

	log.Trace().Interface("sort", sort).Msg("Analizando Sort")
	//recorro y sanitizo el sort
	for i := range sort {
		log.Trace().Str("campo", sort[i]).Msg("Sort")
		prefijo := string(sort[i][0])
		re := regexp.MustCompile("^[0-9a-zA-Z_-]+$")
		if !re.Match([]byte(sort[i])) {
			log.Warn().Str("campo", sort[i]).Msg("Campo Inválido en Sort")
			continue
		}
		if prefijo == "-" {
			sort[i] = sort[i][1:] + " DESC"
		}
		_sort = append(_sort, sort[i])
		log.Trace().Interface("sort", _sort).Msg("Fin de análisis de sort")
	}

	m.sort = _sort
	return m
}

//LeftJoin ...
func (m *dbSelect) LeftJoin(table string) *dbSelect {
	m.leftJoin = table
	return m
}

//On ...
func (m *dbSelect) On(cond string) *dbSelect {
	m.on = cond
	return m
}

//GroupBy ...
func (m *dbSelect) GroupBy(fields ...string) *dbSelect {
	m.groupby = fields
	return m
}

//Limit Establece un límite a la cantidad de registros devueltos por la BD
// Por conveniencia, limit es un string, que puede ser vacío (sin limit)
func (m *dbSelect) Limit(limit string) *dbSelect {
	if limit != "" {
		m.limit = limit
	}
	return m
}

//Offset establece el registro de inicio para el resultado de la consulta a BD
// es un string que puede ser vacío (se ignora)
func (m *dbSelect) Offset(offset string) *dbSelect {
	m.offset = offset
	return m
}

//Where recibe una lista de filtros a aplicar a la sentencia sql
func (m *dbSelect) Where(filters ...string) *dbSelect {
	m.whereSQL = filters
	return m
}

//Exec Ejecuta la sentencia SQL iniciada con la función Select()
func (m *dbSelect) Exec(binds ...interface{}) error {

	// log.Trace().Interface("m", m).Msg("GetAll")

	var limit, offset, orderBy, where, leftjoin, groupby string
	// var whereBinds []interface{}

	if m.limit != "" {
		limit = " LIMIT " + m.limit
		if m.offset != "" {
			offset = " OFFSET " + m.offset
		}
	}

	if m.leftJoin != "" {
		leftjoin = " LEFT JOIN " + m.leftJoin
		if m.on != "" {
			leftjoin += " ON (" + m.on + ")"
		}
	}

	if len(m.sort) > 0 {
		orderBy = " ORDER BY " + strings.Join(m.sort, ", ")
	}

	if len(m.groupby) > 0 {
		groupby = " GROUP BY " + strings.Join(m.groupby, ", ")
	}

	if len(m.whereSQL) > 0 {
		where = " WHERE " + strings.Join(m.whereSQL, " AND ")
	}

	sql := "select " + m.cols + " from " + m.table + leftjoin + where + groupby + orderBy + limit + offset
	log.Trace().Str("sql", sql).Interface("binds", binds).Msg("GetAll")
	data := m.dest.GetData()
	if err := DB.Select(data, sql, binds...); err != nil {
		return err
	}

	var count int
	// if	err = db.Get(&count, "SELECT count(*) FROM place")
	if err := DB.Get(&count, "select count(*) from "+m.table); err != nil {
		return err
	}

	meta := m.dest.GetMeta()
	meta["total"] = count
	// meta["returned"] = len(data.([]Modeler))

	return nil
}

//ParseFiters parsea la lista de filtros y devuelve la lista de SQL y Binds resultantes
// los datos devueltos pueden ser utilizados en la sentecia where de la consulta a BD
func ParseFiters(filters map[string][]string, parsers map[string]Filter) (sql []string, binds []interface{}, err error) {

	for f := range filters {
		log.Trace().Str("filtro", f).Interface("contenido", filters[f]).Msg("Analizando Filtro")

		if filtro, found := parsers[f]; found == true {
			filtro.Parse(filters[f])
			log.Debug().Str("SQL", filtro.SQL).Bool("isValid", filtro.IsValid(filtro.Bind)).Interface("bind", filtro.Bind).Str("filtro", f).Msg("Filtro encontrado")
			if !filtro.IsValid(filtro.Bind) {
				log.Warn().Str("filtro", f).Interface("valor", filtro.Bind).Msg("Filtro Inválido")
				err = errors.New("Filtro Inválido")
				return
			}
			sql = append(sql, filtro.SQL)
			for g := range filtro.Bind {
				binds = append(binds, filtro.Bind[g])
			}
		}
	}

	return
}

//Get Ejecuta una consulta utilizando la clave primaria del modelo ingresado en model
// model debe ser una referencia a una estructura que implemente Modeler
// id es la lista de valores para las claves primarias, la cantidad de elementos debe coincidir con la cantidad de claves primaras del modelo
func Get(model Modeler, id ...interface{}) error {
	sql := "select * from " + model.GetName() + " where "

	pk := model.GetPrimaryKeys()
	var s []string
	for i := range pk {
		s = append(s, pk[i]+"= ?")
	}
	sql += strings.Join(s, " AND ")

	log.Trace().Str("sql", sql).Interface("ids", id).Msg("model.Get")

	data := model.GetData()

	if err := DB.Get(data, sql, id...); err != nil {
		return err
	}

	return nil
}

//Delete elimina un registro de la BD
// model es una estructura que implementa Modeler
// id es una lista de valores para las claves primarias del modelo
func Delete(model Modeler, id ...interface{}) error {

	var err error
	sql := "delete from " + model.GetName() + " where "

	pk := model.GetPrimaryKeys()
	var s []string
	for i := range pk {
		s = append(s, pk[i]+"= ?")
	}
	sql += strings.Join(s, " AND ")

	log.Debug().Str("sql", sql).Msg("model.Delete")

	res, err := DB.Exec(sql, id...)
	if err != nil {
		return err
	}

	affectedRows, err := res.RowsAffected()
	if err != nil {
		return err
	}

	if affectedRows == 0 {
		log.Warn().Interface("id", id).Int64("affectedRows", affectedRows).Msg("No se eliminó ningún elemento")
		return errors.New("No se encontró el recurso específicado")
	}
	return err
}

/* ********************************************************************************************************** */

//TODO implementar transacciones para insert múltiples
//Insert Inicia una sentencia Insert en la BD
func Insert() *dbInsert {
	return &dbInsert{}
}

//dbInsert es un tipo de datos que permite la insersión de regitros en la BD
type dbInsert struct {
	table string
	// model Modeler
	values interface{}
	cols   []string
}

//Into establece la tabla de destino del insert
func (m *dbInsert) Into(table string) *dbInsert {
	m.table = table
	return m
}

//
func (m *dbInsert) Columns(cols ...string) *dbInsert {
	m.cols = cols
	return m
}

//Values Valores a insertar
// Esta función debe ser la última en llamarse, ya que es la que ejecuta el comando SQL
func (m *dbInsert) Values(values ...interface{}) (*int64, error) {
	// m.values = values
	var binds []string
	for range values {
		binds = append(binds, "?")
	}

	sql := "INSERT INTO " + m.table + "(" + strings.Join(m.cols, ",") + ") VALUES (" + strings.Join(binds, ", ") + ")"

	log.Debug().Str("sql", sql).Interface("binds", values).Msg("model.Insert")

	res, err := DB.Exec(sql, values...)
	if err != nil {
		return nil, err
	}

	affectedRows, err := res.RowsAffected()
	if err != nil {
		return nil, err
	}

	if affectedRows == 0 {
		log.Warn().Int64("affectedRows", affectedRows).Msg("No se insertó ningún elemento")
		return nil, errors.New("No se encontró el recurso específicado")
	}

	lastID, err := res.LastInsertId()
	if err != nil {
		return nil, err
	}

	return &lastID, nil
}

//Update Inicia una sentencia Insert en la BD
func Update(table string) *dbUpdate {
	return &dbUpdate{
		table: table,
	}
}

//dbUpdate es un tipo de datos que permite la insersión de regitros en la BD
type dbUpdate struct {
	table  string
	cols   []string
	values interface{}
}

//Set campos a modificar
func (m *dbUpdate) Set(cols ...string) *dbUpdate {
	m.cols = cols
	return m
}

//Values valores a actualizar
func (m *dbUpdate) Values(values ...interface{}) *dbUpdate {
	m.values = values
	return m
}

// Where ejecuta el Update sobre las claves primarias especificadas
// el primer parámetro es la lista de campos que son claves primarias
// El segundo campo es la lista de valores para esas claves primarias
// la función utiliza estos valores para armar el string "WHERE <clave_1> = ? AND <clave_"> = ? ...."
// Esta función debe ser la última en llamarse, ya que es la que ejecuta el comando SQL
func (m *dbUpdate) Where(pks []string, v []interface{}) error {
	// var binds []string
	mValues := m.values.([]interface{})
	var values []interface{}
	var cols []string
	for i := range mValues {
		cols = append(cols, m.cols[i]+" = ?")
		values = append(values, mValues[i])
	}

	var cond []string
	for i := range pks {
		cond = append(cond, pks[i]+" = ?")
	}

	values = append(values, v...)

	sql := "UPDATE " + m.table + " SET " + strings.Join(cols, ",") + " WHERE " + strings.Join(cond, " AND ")

	log.Debug().Str("sql", sql).Interface("binds", values).Msg("model.Update")

	res, err := DB.Exec(sql, values...)
	if err != nil {
		return err
	}

	affectedRows, err := res.RowsAffected()
	if err != nil {
		return err
	}

	if affectedRows == 0 {
		log.Warn().Int64("affectedRows", affectedRows).Msg("No se actualizó ningún elemento")
	}

	return nil
}
