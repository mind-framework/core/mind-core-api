//Package model ...
package model

import (
	"errors"
	"regexp"
	"strings"
)

//Filter es una estructura que contiene la regla o sentencia SQL a aplicar para un filtro
type Filter struct {
	SQL  string   `json:"-"`
	Bind []string `json:"-"`

	//Descripcion es utilizada para brindar al usuario una descripción del filtro
	Description string `json:"desc"`

	//ValidValues contiene una lista de los valores válidos para el filtro
	// estos valores son analizados antes de realizar la ejecución de la consulta
	ValidValues []string `json:"valid"`

	Parser FilterParser `json:"-"`
}

//Parse Ejecuta el Parser para el Filtro y almacena la sentencia SQL y los binds
func (f *Filter) Parse(values []string) error {
	sql, bind, err := f.Parser(values)
	if err != nil {
		return err
	}

	f.SQL = sql
	f.Bind = bind
	return nil
}

//IsValid devuelve true si los valores recibidos para este filtro son válidos
// Internamente esta función recorre el slice []ValidValues
func (f *Filter) IsValid(values []string) bool {
	for i := range values {
		value := values[i]
		for j := range f.ValidValues {
			log.Trace().Interface("filtro", f.ValidValues[j]).Interface("value", value).Msg("Analizando Filtro")
			re := regexp.MustCompile(f.ValidValues[j])
			if re.Match([]byte(value)) {
				log.Warn().Interface("filtro", f.ValidValues[j]).Interface("value", value).Msg("Filtro Inválido")
				return true
			}
		}
	}
	return false
}

//FilterParser es una interfaz que define las funciones del tipo Parser
type FilterParser func([]string) (string, []string, error)

//NewGenericFilterParser devuelve un FilterParser para el campo especificado
// Este parser analiza los valores recibidos para el filtro y establece las siguientes reglas:
//	- si se recibe una slice de valores mayor a 1 se genera un sql con el formato 'campo in ?'
//  - si se recibe un slice con un solo valor, se genera un sql con el formato 'campo = ?'
//  - si el valor contiene un "*" al inicio y/o al final, se devuelve un sql con el formato 'campo like ?'
// @param campo es el nombre del campo que se utilizará en la generación del SQL
// @param forceLike fuerza la utilziación de 'like' aunque corresponda un '='
func NewGenericFilterParser(campo string, forceLike bool) FilterParser {
	return func(values []string) (string, []string, error) {
		var valor []string
		var sql string
		switch {
		case len(values) == 1:
			var sentencia = " = "
			v := values[0]
			if forceLike || string(v[0]) == "*" || string(v[len(v)-1]) == "*" { //reemplazo el * por un %
				sentencia = " LIKE "
				v = "%" + strings.Replace(v, "*", "", -1) + "%"
			}
			valor = append(valor, v)
			sql = campo + sentencia + "?"
		case len(values) > 1:
			sql = campo + " IN("
			var s []string
			// var b []string
			for range values {
				s = append(s, "?")
			}
			sql = sql + strings.Join(s, ",") + ")"
			valor = values
		default:
			return sql, valor, errors.New("No se recibió un valor para el filtro")
		}

		return sql, valor, nil
	}

}
