//Package model contiene las funciones e interfaces necesarios para la ejecución de los las acciones sobre los modelos de datos
package model

import (
	"errors"

	_log "github.com/rs/zerolog/log"
)

var log = _log.With().Str("pkg", "model").Logger()

//NewModeler interfaz para fundiones que devuelven un Modeler
type NewModeler func() Modeler

var (
	//ErrorNotImplemented Indica que el metodo no está implementado
	ErrorNotImplemented error = errors.New("Mètodo No Implementado")
)

//Modeler es un interfaz con los métodos comunes de un Model
type Modeler interface {
	GetName() string
	GetData() interface{}
	GetMeta() map[string]interface{}
	GetPrimaryKeys() []string

	//GetFilters devuelve la lista de filtros permitidos por este modelo
	// GetFilter(nombre string) (Filter, bool)

	List(sort []string, limit string, offset string, filters map[string][]string) (Modeler, error)
	Get(id ...interface{}) (Modeler, error)
	Delete(id ...interface{}) error
	Insert(data interface{}) (Modeler, error)
	Update(data interface{}, id ...interface{}) (Modeler, error)
}

//Model es una estructura base que debe ser incluida en los modelos que utilicen las funciones de este paquete
// Esta estructura implementa algunas de las funciones de Modeler
type Model struct {
	Name        string                 `json:"-"` //Nombre del Modelo (tabla)
	PrimaryKeys []string               `json:"-"`
	Data        interface{}            `json:"data"`
	Meta        map[string]interface{} `json:"meta"`
}

//GetName devuelve el nombre de la tabla/modelo
func (m *Model) GetName() string {
	return m.Name
}

//GetPrimaryKeys devuelve la lista de claves primarias del modelo
func (m *Model) GetPrimaryKeys() []string {
	return m.PrimaryKeys
}

//GetData devuelve el puntero al la variable que contiene el resultado del query
func (m *Model) GetData() interface{} {
	return m.Data
}

//GetMeta devuelve el puntero al la variable que contiene los metadatos del query
func (m *Model) GetMeta() map[string]interface{} {
	return m.Meta
}
