package events

import (
	_log "github.com/rs/zerolog/log"
)

var log = _log.With().Str("pkg", "channel").Logger()

//NewBroker devuelve un nuevo Broker de canales
func NewBroker() *Broker {
	return &Broker{
		clients:     make(map[chan interface{}]bool),
		subscribe:   make(chan (chan interface{})),
		Unsubscribe: make(chan (chan interface{})),
		Messages:    make(chan interface{}),
	}
}

// Broker is responsible
// for keeping a list of which clients are currently attached
// and broadcasting events (messages) to those clients.
type Broker struct {
	// Create a map of clients, the keys of the map are the channels
	// over which we can push messages to attached clients.  (The values
	// are just booleans and are meaningless.)
	//
	clients map[chan interface{}]bool

	// Channel into which new clients can be pushed
	//
	subscribe chan chan interface{}

	// Channel into which disconnected clients should be pushed
	//
	Unsubscribe chan chan interface{}

	// Channel into which messages are pushed to be broadcast out
	// to attahed clients.
	//
	Messages chan interface{}
}

//Subscribe crea un canal de subscripcion al broker
func (b *Broker) Subscribe() chan interface{} {
	messageChan := make(chan interface{})
	b.subscribe <- messageChan
	return messageChan
}

// Start This Broker method starts a new goroutine.  It handles
// the addition & removal of clients, as well as the broadcasting
// of messages out to clients that are currently attached.
//
func (b *Broker) Start() {

	// Start a goroutine
	//
	go func() {

		// Loop endlessly
		//
		for {

			// Block until we receive from one of the
			// three following channels.
			select {

			case s := <-b.subscribe:

				// There is a new client attached and we
				// want to start sending them messages.
				b.clients[s] = true
				log.Debug().Msg("Agregado Nuevo Cliente")

			case s := <-b.Unsubscribe:

				// A client has dettached and we want to
				// stop sending them messages.
				delete(b.clients, s)
				close(s)
				log.Debug().Msg("Cliente eliminado")
			case msg := <-b.Messages:

				// There is a new message to send.  For each
				// attached client, push the new message
				// into the client's message channel.
				for s := range b.clients {
					s <- msg
				}
				log.Debug().Msg("Broadcast message to clients")
			}
		}
	}()
}
