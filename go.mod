module gitlab.com/mind-framework/core/mind-core-api

go 1.15

require (
	github.com/go-chi/chi v1.5.1
	github.com/go-chi/render v1.0.1
	github.com/jmoiron/sqlx v1.2.0
	github.com/rs/zerolog v1.20.0
)
